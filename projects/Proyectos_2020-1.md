---
# Proyectos 2020-1. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Detección de cáncer de colon en imágenes histopatológicas](#proy1)
2. [Detection of Geometric Figures](#proy2)
3. [Transferencia de Aprendizaje y Ajuste Fino para la Clasificación de Lesiones de Piel con el Conjunto de Datos HAM10000 mediante Redes Neuronales Convolucionales](#proy3)
4. [Reconocimiento De Genero Por Medio De Grabaciones De Voz](#proy4)
5. [Clasificador de Mensajes Spam](#proy5)
6. [Predicción de equipo ganador por medio de IA](#proy6)
7. [Sonido Respiratorio: Uso de grabaciones para detectar enfermedades respiratorias](#proy7)
8. [Sentiment analysis of a review by NLP (Natural Language Processing) and Neural Networks](#proy8)
9. [Chattervis](#proy9)
10. [Clasificación del estado de salud del feto mediante cardiotocografía](#proy10)
11. [BIRD CLASSIFIER](#proy11)
12. [Clasificación de la fortaleza de una contraseña](#proy12)
13. [Clasificación de anomalías de la columna vertebral Por medio de datos obtenidos de exámenes de Rayos X](#proy13)
14. [Análisis y clasificación sobre el covid-19 en Colombia](#proy14)
15. [Identificación de Asteroides Potencialmente Peligrosos](#proy15)
16. [Machine Learning con estadísticas e imágenes de Pokémon](#proy16)
17. [Procesamiento de imágenes de blanco y negro a color, usando un dataset de gatitos](#proy17)
18. [Clasificador de noticias falsas mediante NLP y análisis de texto](#proy18)
19. [Sistema de Recomendación Musical](#proy19)

---

## Detección de cáncer de colon en imágenes histopatológicas <a name="proy1"></a>

**Autores: Miguel Angel Duarte Delgado**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2142665/2142665.jpg" style="width:700px;">

**Objetivo: Implementación de una red neuronal convolucional para detectar si un individuo padece cáncer de colon mediante un análisis de imágenes histopatológicas de tejido del colon.**

- Dataset: Lung and Colon Cancer Histopathological Image Dataset (LC25000)
- Modelo: Convolutional Neural Network, SVC, Random Forest Classifier, Gaussian Naive Bayes, Decision Tree Classifier

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2142665/Project.ipynb) [(video)](https://www.youtube.com/watch?v=-HKQd_fuSzI) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2142665/2142665.pdf)

---

## Detection of Geometric Figures <a name="proy2"></a>

**Autores: Iván Daniel Maestre Muza, Laura Marcela Mantilla Romero, Daniel Felipe Rueda Mariño**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170112-2162130-2170135/2170112-2162130-2170135.jpg" style="width:700px;">

**Objetivo: En este proyecto se trató la clasificación de figuras geométricas, como lo son círculos, cuadrados, triángulos y estrellas, haciendo uso de una estructura de red neuronal convolucional para la clasificación de estas figuras.**

- Dataset: Four Shapes
- Modelo: Red neuronal convolucional, LeNet

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170112-2162130-2170135/2170112-2162130-2170135.ipynb) [(video)](https://www.youtube.com/watch?v=DCKursZu-kM) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170112-2162130-2170135/2170112-2162130-2170135.pdf)

---

## Transferencia de Aprendizaje y Ajuste Fino para la Clasificación de Lesiones de Piel con el Conjunto de Datos HAM10000 mediante Redes Neuronales Convolucionales <a name="proy3"></a>

**Autores: Camilo Andrés Calderón Carrillo, Jessica Paola Escobar Pérez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170090-2171713/2170090-2171713.jpg" style="width:700px;">

**Objetivo: Este trabajo propone una tarea de clasificación de siete clases de lesiones cutáneas del conjunto de datos HAM10000 utilizando aprendizaje profundo sobre la red neuronal convolucional ResNet50 usando las técnicas de aumento de datos, transferencia de aprendizaje y ajuste fino.**

- Dataset: HAM10000
- Modelo: Red neuronal convolucional, ResNet50

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170090-2171713/ResNet50.ipynb) [(video)](https://www.youtube.com/watch?v=17zbxotoFeM) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170090-2171713/2170090-2171713.pdf)

---

## Reconocimiento De Genero Por Medio De Grabaciones De Voz <a name="proy4"></a>

**Autores: Hazel David Pinzón Uribe, Mateo Orozco Ardila, Nicolas Galvan Alvarez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2163022-2170111-2170104/2163022-2170111-2170104.jpeg" style="width:700px;">

**Objetivo: El proyecto tiene como propósito la predicción del genero de una persona mediante una grabación de voz.**

- Dataset: Voice Gender
- Modelo: SVM, Decision Tree, Random Forest, Naive Bayes, NN

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2163022-2170111-2170104/Prototipo.ipynb) [(video)](https://www.youtube.com/watch?v=V5KvEI_B-Xc) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2163022-2170111-2170104/2163022-2170111-2170104.pdf)

---

## Clasificador de Mensajes Spam <a name="proy5"></a>

**Autores: Christian Alejandro Rengifo Mejia, Diego Fernando Gonzales Ortiz**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2162133-2151852/2162133-2151852.jpg" style="width:700px;">

**Objetivo: Evitar la recepción de mensajes SPAM de forma automática por medio de una herramienta que reconoce y oculta dichos mensajes sin la necesidad de un esfuerzo manual.**

- Dataset: SMS Spam Collection Dataset
- Modelo: Bag of words, TF-IDF, Multinomial, Naive Bayes, Gaussian Naive Bayes, Support Vector Classifier, Stochastic Gradient Descent

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2162133-2151852/2162133-2151852.ipynb) [(video)](https://www.youtube.com/watch?v=clEpU7_UMVs) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2162133-2151852/2162133-2151852.pdf)

---

## Predicción de equipo ganador por medio de IA <a name="proy6"></a>

**Autores: Cristian Guillermo Serrano Acevedo, Joseph Fabian Basto Cuadros, Liliana Paola Castellanos Pinzón**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170127-2170081-2170095/2170127-2170081-2170095.jpg" style="width:700px;">

**Objetivo: En este proyecto se trabaja un problema de predicción del equipo ganador dentro de una partida de League of Legends por medio de IA. Para ello, se usan métodos, estimadores, y parámetros que predicen el ganador de dichas partidas.**

- Dataset: League of Legends Diamond Ranked Games (10 min)
- Modelo: Decision Tree Classifier, Support Vector Classifier, Multinomial Naive-Bayes

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170127-2170081-2170095/2170127-2170081-2170095.ipynb) [(video)](https://www.youtube.com/watch?v=pjrGjyZV0IY) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170127-2170081-2170095/2170127-2170081-2170095.pdf)

---

## Sonido Respiratorio: Uso de grabaciones para detectar enfermedades respiratorias <a name="proy7"></a>

**Autores: Javier Andres Adarme Davila, Edison Camilo Porras Melgarejo**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2163021-2162923/2163021-2162923.jpg" style="width:700px;">

**Objetivo: Este proyecto se basa en la clasificación de los audios para detectar si la persona posee una de las siguientes enfermedades; el asma, urticaria, COPD, LRTI, Bronchiectasis, neumonía y bronquiolitis.**

- Dataset: ICBHI Scientiﬁc Challenge database
- Modelo: Random Forest, Naive Gaussian, Decision Tree Classifier, SVC, Redes

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2163021-2162923/2163021-2162923.ipynb) [(video)](https://www.youtube.com/watch?v=jZfZ3OE7E9g) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2163021-2162923/2163021-2162923.pdf)

---

## Sentiment analysis of a review by NLP (Natural Language Processing) and Neural Networks <a name="proy8"></a>

**Autores: Santiago Andrés Castro Duitama, Christian Ruiz Lagos, Kevin Joel Dlaikan Castillo**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170134-2152204-2160090/2170134-2152204-2160090.jpg" style="width:700px;">

**Objetivo: El proyecto consistió en predecir la opinión basada en una review de una película.**

- Dataset: IMDB Dataset of 50K Movie Reviews
- Modelo: Natural Language Processing (NLP), Red neuronal recurrente

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170134-2152204-2160090/2170134-2152204-2160090.ipynb) [(video)](https://www.youtube.com/watch?v=1r4MwqbBRxY) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170134-2152204-2160090/2170134-2152204-2160090.pdf)

---

## Chattervis <a name="proy9"></a>

**Autores: Jhon Stewar nuñez Castellanos, Samuel Yesid Cadena Pinilla**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2171066-2171399/2171066-2171399.jpg" style="width:700px;">

**Objetivo: se creó un ChatBot adaptado para funcionar en Discord, con el fin de recrear conversaciones cortas con un usuario, donde dicha conversaciones están parametrizadas en un dataset por medio del cual se hace una regresión de lo que el usuario diga para devolver la respuesta más acertada.**

- Dataset: chat dataset
- Modelo: Regression

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2171066-2171399/2171066-2171399.zip) [(video)](https://www.youtube.com/watch?v=j2qBOhjqUic) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2171066-2171399/2171066-2171399.pdf)

---

## Clasificación del estado de salud del feto mediante cardiotocografía <a name="proy10"></a>

**Autores: Fredy Alejandro Mendoza Lopez, Orlando Alberto Moncada Rodríguez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170133-2170116/2170133-2170116.jpg" style="width:700px;">

**Objetivo: se determinó un modelo que sirve como apoyo al personal de esta área. El modelo se escogió a partir de un grupo de estimadores entrenados con un dataset que cuenta con más de 2000 cardiotocografías. Este, cuenta con un 94% de precisión y se ajusta a las métricas de evaluación relevantes en medicina.**

- Dataset: UCI Cardiotocography
- Modelo: Decision Tree, SVM, Random Forest y Neural Network

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170133-2170116/2170133-2170116.ipynb) [(video)](https://www.youtube.com/watch?v=PlOaR2eNnt4) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170133-2170116/2170133-2170116.pdf)

---

## BIRD CLASSIFIER <a name="proy11"></a>

**Autores: Carlos Alfonso Estevez Carvajal, Andres camilo Hernandez Arias, Duvan Fernando Pinto Diaz**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2160042-2130284-2161889/2160042-2130284-2161889.jpg" style="width:700px;">

**Objetivo: es un clasificador de imágenes de aves, su principal objetivo es el de identificar, el tipo y la especie a la cual pertenece el ave mostrada en la imagen que le pasemos, y para esto se uso y se adapto un Dataset con una variedad muy grande de imágenes de diferentes especies y fue desarrollando usando una red convolucional y otros tipos de clasificadores.**

- Dataset: Bird Species
- Modelo: Convolutional neural networks

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2160042-2130284-2161889/2160042-2130284-2161889.ipynb) [(video)](https://www.youtube.com/watch?v=5MiQ--pMa90) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2160042-2130284-2161889/2160042-2130284-2161889.pdf)

---

## Clasificación de la fortaleza de una contraseña <a name="proy12"></a>

**Autores: Jonathan Tovar Diaz**

<!--<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2172027/2172027.jpg" style="width:700px;">-->

**Objetivo: se ha elaborado un clasificador de contraseñas en donde según un banco de datos de +600.000 contraseñas previamente catalogadas se puede determinar que tan segura es una contraseña en escala de 0 a 2 en donde 0 es débil y 2 es fuerte.**

- Dataset: password dataset
- Modelo: XGBClassifier, RandomForestClassifer

<!--[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2172027/2172027.ipynb) -->
[(video)](https://www.youtube.com/watch?v=AfWdOUuFGP8) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2172027/2172027.pdf)

---

## Clasificación de anomalías de la columna vertebral Por medio de datos obtenidos de exámenes de Rayos X <a name="proy13"></a>

**Autores: Julian Colmenares, Laura Vargas, Juan Leon**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2132173-2151484-2162144/2132173-2151484-2162144.jpg" style="width:700px;">

**Objetivo: se procedió en la búsqueda del dataset que permitiera construir el clasificador de anomalías de columna vertebral, una vez encontrado se procedió a hacer el tratamiento de los datos que este contenía, se dividió de la mejor manera posible para tener un número equilibrado de elementos de cada clase, es decir, diagnósticos de personas con condiciones normales y anormales, para que así el clasificador pudiese trabajar óptimamente. Para un mejor resultado se realizaron pruebas con los mismos datos en los diferentes tipos de clasificadores disponibles en la librería sklearn de python, además se hizo uso de la técnica de cross validation para posteriormente compararlas. una vez obtenidos los resultados se escogieron los clasificadores más confiables para facilitar el diagnóstico de anomalías vertebrales.**

- Dataset: VertebralColumnDataSet
- Modelo: GaussianNB, DecisionTree, SVC

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2132173-2151484-2162144/2132173-2151484-2162144.ipynb) [(video)](https://www.youtube.com/watch?v=mddoF55sGo8) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2132173-2151484-2162144/2132173-2151484-2162144.pdf)

---

## Análisis y clasificación sobre el covid-19 en Colombia <a name="proy14"></a>

**Autores: Sergio Raúl Roa Ortiz, Edinson Jahir Rodriguez Garces, Mauren Lorena Cobos Becerra**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2162887-2162126-2162112/2162887-2162126-2162112.jpg" style="width:700px;">

**Objetivo: se consideraron datos estadísticos del virus llamado Covid-19 para aplicarle los temas de regresión y clasificación adoptados dentro de la inteligencia artificial. Se decidió analizar la manera en que este virus afectó a Colombia y encontrar cuales fueron las poblaciones más afectadas. Se tomó un día específico del mes de julio de 2020 como tope para analizar los datos y realizar la respectiva clasificación y regresión de cada una de las columnas utilizadas, tales como: edad, estado, sexo, entre otros.**

- Dataset: Estado de casos de Coronavirus COVID-19 en Colombia (datos abiertos)
- Modelo: GaussianNB, Decision Tree, Random Forest

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2162887-2162126-2162112/2162887-2162126-2162112.ipynb) [(video)](https://www.youtube.com/watch?v=jD2VZQ0iOrg) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2162887-2162126-2162112/2162887-2162126-2162112.pdf)

---

## Identificación de Asteroides Potencialmente Peligrosos <a name="proy15"></a>

**Autores: Kevin Javier Lozano Galvis, Brayan Rofoldo Barajas Ochoa**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2172016-2170688/2172016-2170688.jpg" style="width:700px;">

**Objetivo: Identificar asteroides potencialmente peligrosos usando un enfoque de inteligencia artificial.**

- Dataset: NASA JPL Asteroid Dataset
- Modelo: Linear SVM, Decision Tree Classifier, AdaBoost, KMeans

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2172016-2170688/2172016-2170688.ipynb) [(video)](https://www.youtube.com/watch?v=zV8TeB01wJc) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2172016-2170688/2172016-2170688.pdf)

---

## Machine Learning con estadísticas e imágenes de Pokémon <a name="proy16"></a>

**Autores: David Felipe Rojas, Julián Felipe Tolosa**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170136-2170107/2170136-2170107.jpg" style="width:700px;">

**Objetivo: se analizaron diversos datos e imágenes de la famosa franquicia "Pokémon" mediante el uso de distintos modelos de regresión y reconocimiento de imágenes respectivamente. En primer lugar, se emplearon los modelos de regresión para predecir el porcentaje de partidas ganadas por cierto Pokémon dependiendo de sus estadísticas. Por otro lado, se creó un modelo de red neuronal convolucional entrenado y testeado con imágenes de diversos Pokémon, el cual fue posteriormente combinado con el modelo de red neuronal MobileNetV2.**

- Dataset: Pokemon Weedle's Cave, pokemon-image-dataset
- Modelo: SVM, Decision Tree, Random Forest, MobileNetV2

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170136-2170107/2170136-2170107.ipynb) [(video)](https://www.youtube.com/watch?v=uSAz0vaS2QI) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170136-2170107/2170136-2170107.pdf)

---

## Procesamiento de imágenes de blanco y negro a color, usando un dataset de gatitos <a name="proy17"></a>

**Autores: Andrés Felipe Uribe García, Julián Orlando Rodríguez Villamizar**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170137-2160793/2170137-2160793.jpg" style="width:700px;">

**Objetivo: Este proyecto tiene como objeto el análisis de imágenes. Para esto en un principio nos basaremos en imágenes de gatos, entrarán imágenes en blanco y negro que pasarán por el modelo, después nos devolverá la imagen a color como ella lo predijo. En otra carpeta tenemos la "Ground Truth" que vendría siendo la imagen a color original.**

- Dataset: Dogs & Cats Images
- Modelo: Tensorflow, Numpy, imageio

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2170137-2160793/2170137-2160793.ipynb) [(video)](https://www.youtube.com/watch?v=OUmDDnnl3_E) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2170137-2160793/2170137-2160793.pdf)

---

## Clasificador de noticias falsas mediante NLP y análisis de texto <a name="proy18"></a>

**Autores: Miguel Angel Oquendo Rincon, Carlos Alberto Palencia Pombo**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2161341-2161342/2161341-2161342.jpg" style="width:700px;">

**Objetivo: se desarrolló un algoritmo capaz de analizar y determinar las probabilidades que tiene una noticia de ser falsa o verdadera. La metodología consistió en el análisis de las palabras que se usan en los títulos y texto mediante clasificación binaria apoyado por una red neuronal, mostrando mayor efectividad al analizar la primer opción.**

- Dataset: Fake and real news dataset
- Modelo: NLP, embedding, redes neuronales

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2161341-2161342/2161341-2161342.ipynb) [(video)](https://www.youtube.com/watch?v=BzsBxAeldEA) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2161341-2161342/2161341-2161342.pdf)

---

## Sistema de Recomendación Musical <a name="proy19"></a>

**Autores: Camilo Andres Moreno Pinto**

<!--<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2180049/2180049.jpg" style="width:700px;">-->

**Objetivo: se desarrolló un sistema que permite la recomendación de dos canciones similares a la canción que el usuario le ingrese, esto permite conocer más canciones sobre un genero musical u otro. Por el momento las canciones que se usan para la recomendación son canciones preexistentes en el disco duro del usuario.**

- Dataset: N/A
- Modelo: espectrogramas de mel, redes convolucionales

<!--[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/blob/master/projects/sources/2020-1/2180049/2180049.ipynb)-->
[(video)](https://www.youtube.com/watch?v=-CAEEndB-SE) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/-/raw/master/projects/sources/2020-1/2180049/2180049.pdf)

---
