# Inteligencia Artificial I 2021-1

## Bienvenidos!

<img src="/imgs/img_IA.jpg" style="width:400px;">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local. 


## Calificación
- 40% Talleres (Problemsets)
- 30% Parciales (Quizes) 
- 30% Proyecto funcional IA 

## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres ser desarrollará en casa, dentro de las fechas establecidas en el cronograma. 


## Parciales (Quizes)

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Los apuntes y notebooks del curso se pueden utilizar (curso del repositorio). 


## Proyecto funcional IA

- **Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **Prototipo (PRE-SUS PROJ)**: En este item se considera como esta estructurado el proyecto y se espera una nivel razonable de funcionalidad.

- **Presentación**:
    - Imagen relacionada (800 x 300) con la siguiente información: título del proyecto e información de los estudiantes<br>
    - Video corto (Máximo 5 minutos) (ENTREGAR EL ARCHIVO DE VIDEO y también alojarlo en youtube)<br>
    - Archivo de las diapositivas

- **Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

- **REPOSITORIO DEL PROYECTO**
    - Todos los archivos relacionados con el proyecto deben alojarse en un repositorio de los integrantes del estudainte
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores: Separados por comas
        - Objetivo: Una frase
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Enlaces del código, video, y repositorio
    


**UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**


## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 Abr06-Abr07    Intro                Python-overall            
     W02 Abr13-Abr14    Python-Numpy         Python-Vis   
     W03 Abr20-Abr21    Pandas               Pandas 
     W03 Abr27-Abr28    Estadística          --- 
     ...................    PERIODO SIN CLASES      ..................... 
     W04 Ago03-Ago04    REPASO               REPASO    
     W05 Ago10-Ago11    Estadística          Estadística           
     W06 Ago17-Ago18    Intro A.M.S.         Clasificación A.M.S.    
     W07 Ago24-Ago25    Regresión A.M.S.     Métodos A.M.S.         Parcial 1
     W08 Ago31-Sep01    Métodos A.M.S.       Aplicación A.M.S.                 
     W09 Sep07-Sep08    Deep Learning        N10. Deep Learning
     W10 Sep14-Sep15    N11. Deep Learning   Aclaraciones               
     W11 Sep21-Sep22    Aclaraciones         Intro A.M.N.S.         parcial 2
     W12 Sep28-Sep29    PRE-SUS PROJ         PRE-SUS PROJ
     W13 Oct05-Oct06    K-means A.M.N.S.     DBScan A.M.N.S 
     W14 Oct12-Oct13    Planning and S.      Genetic Alg.
     W15 Oct19-Oct20    Simulated anne.      Aclaraciones           Parcial 3
     W16 Oct26-Oct27    SUS PROJ             SUS PROJ               
   

     Ago 27 -           -> Registro primera nota
     Ago 29 -           -> Último día cancelación materias
     Oct 22             -> Finalización clase
     Oct 25 - Oct 29    -> Evaluaciones finales
     Nov 03 -           -> Registro calificaciones finales
    


**ACUERDO n.° 211 DE 2021 - 23 de Julio**
[Calendario academico](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2020/acuerdoAcad434_2020.pdf)


**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**

**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS**

**DEADLINE DE LOS PROBLEMSETS SERÁ EL DIA DE CADA PARCIAL**

